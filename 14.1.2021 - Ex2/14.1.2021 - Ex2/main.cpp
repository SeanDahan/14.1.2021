#include <iostream>
#include "functions.h"

template <class T>
int compare(T param1, T param2)
{
	if (param1 == param2)
		return 0;
	else if (param1 < param2)
		return 1;
	else
		return -1;
}

template <class T>
void bubbleSort(T* param1, int size)
{
	T temp;
	for (int i = 0; i < size; i++) {
		        //flag to detect any swap is there or not
		for (int j = 0; j < size - i - 1; j++) {
			if (param1[j] > param1[j + 1]) {       //when the current item is bigger than next
				
				temp = param1[j];
				param1[j] = param1[j + 1];
				param1[j + 1] = temp;
				
			}
		}
		       // No swap in this pass, so param1 is sorted
	}
}

template <class T>
void printArray(T* param1, int size)
{
	for (size_t i = 0; i < size; i++)
	{
		std::cout << param1[i] << std::endl;
	}
}


std::ostream& operator<<(std::ostream& os, const Member& mem)
{
	// TODO: insert return statement here
	return os << mem.num;
}

Member::Member()
{
}

bool Member::operator<(const Member& other) const
{
	return this->num < other.num;
}

bool Member::operator>(const Member& other) const
{
	return this->num > other.num;
}

bool Member::operator==(const Member& mem) const
{
	return this->num == mem.num;
	
}




int main() {

//check compare
	std::cout << "correct print is 1 -1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;
	std::cout << compare<char>('a','c') << std::endl;
//check bubbleSort
	
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	char charArr[arr_size] = { 'a','d','c','b','e' };
	Member memArr[arr_size] = { Member(1),Member(3),Member(2),Member(5),Member(4) };
	bubbleSort<char>(charArr, arr_size);
	bubbleSort<double>(doubleArr, arr_size);
	bubbleSort<Member>(memArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	printArray<char>(charArr, arr_size);
	printArray(memArr, arr_size);
	std::cout << std::endl;
	
	system("pause");
	return 1;
}