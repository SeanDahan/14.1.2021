#pragma once
#include <iostream>

template <class T>
int compare(T param1, T param2);
template <class T>
void bubbleSort(T* param1, int size);
template <class T>
void printArray(T* param1, int size);

class Member
{
public:

	Member(int num1) { num = num1; };
	Member();
	int num;

	friend std::ostream& operator<<(std::ostream& os, const Member& mem);
	bool operator<(const Member& other) const;
	bool operator>(const Member& other) const;
	bool operator==(const Member& mem) const;
};