#pragma once
#include <iostream>
template <class T>
class BSNode
{
public:
	//C'tor
	//template <class T>
	BSNode(T data);
	//template <class T>
	BSNode(const BSNode& other);

	//D'tor
	~BSNode();//Distructor

	//Methods
	//template <class T>
	void insert(T value);//Insert a value based on the law's of BST, if the value is present in the tree the function will not do anything
	
	//Copy
	//BSNode& operator=(BSNode other);//Deep copying
	BSNode<T>& operator=(BSNode* other);

	//Checkers
	bool isLeaf() const;//Returns if the tree got no sides
	bool search(T value) const;//Returns if the value is present in the tree

	//Getters
	T getData() const;//Returns the value of the intersection
	BSNode* getLeft() const;//Returns the left side
	BSNode* getRight() const;//Returns the right side
	int getHeight() const;//Returns the height of the tree based on the current intersection
	int getDepth(const BSNode& root) const;//Returns the depth relative to the root given as a parameter
	bool hasLeft() const;
	bool hasRight() const;
	//Void
	void printNodes() const;
	void setHasRight(bool B);
	void setHasLeft(bool B);

	//Helpers
	friend void swap(BSNode& first, BSNode& second);
	

private:
	T _data;
	BSNode* _left;
	BSNode* _right;
	bool _hasLeft;
	bool _hasRight;
	int _count; //for question 1 part B
				
	//Helpers

	int getCurrNodeDistFromInputNode(const BSNode* node) const; //auxiliary function for getDepth
};




