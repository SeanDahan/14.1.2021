
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
//#include "printTreeToFile.h"
#include "BSNode.h"
#include "BSNode.cpp"
using std::cout;
using std::endl;
#define ARRAYSIZE 15
int main()
{

	BSNode<std::string>* bbbs;
	BSNode<std::string>* bbs;
	BSNode<std::string>* bs = new BSNode<std::string>("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("4");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	bbs = new BSNode<std::string>(*bs);
	
	bbbs = bbs;
	bbs->printNodes();
	bbs->insert("20");
	std::cout << "_________\n";
	bbs->printNodes();
	std::cout << "_________\n";
	bs->printNodes();
	std::cout << "_________\n";
	bbbs->printNodes();
	//bbbs->printNodes();
	int* arr = new int[ARRAYSIZE];
	std::string* cArr = new std::string[ARRAYSIZE];
	for (size_t i = 0; i < ARRAYSIZE; i++)
	{
		arr[i] = i * 2 / (i + 1) + i;
		cArr[i] = i * 2 / (i + 1) + 97 + i;

	}
	BSNode<int>* intBs = new BSNode<int>(arr[0]);
	BSNode<std::string>* charBs = new BSNode<std::string>(cArr[0]);
	for (size_t i = 0; i < ARRAYSIZE; i++)
	{
		std::cout << arr[i] << " " << cArr[i] << std::endl;
	}
	for (size_t i = 1; i < ARRAYSIZE; i++)
	{
		intBs->insert(arr[i]);
		charBs->insert(cArr[i]);
	}
	std::cout << "_____________________\n";
	intBs->printNodes();
	charBs->printNodes();

	/*cout << "Tree height: " << bs->getHeight() << endl;
	cout << "Search for 5: " << bs->search(4) << endl;
	cout << "depth of node with abc depth: " << bs->getLeft()->getDepth(*bs) << endl;
	cout << "depth of node with def depth: " << bs->getRight()->getRight()->getDepth(*bs) << endl;
	bs->printNodes();*/
	
	//std::string textTree = "BSTData.txt";
	//printTreeToFile(bs, textTree);

	//system("BinaryTree.exe");
	system("pause");
	//remove(textTree.c_str());
	delete bs;
	delete bbs;
	delete bbbs;
	delete[] arr;
	delete[] cArr;
	delete charBs;
	delete intBs;
	return 0;
}

