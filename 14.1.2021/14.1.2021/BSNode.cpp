#include "BSNode.h"
#include <iostream>
using std::max;
template <class T>
BSNode<T>::BSNode(T data)
{
	this->_data = data;
	this->_hasLeft = false;
	this->_hasRight = false;
	this->_count = 1;
}
template <class T>
BSNode<T>::BSNode(const BSNode& other) :_left(nullptr), _right(nullptr), _data(other._data), _count(other._count), _hasRight(other._hasRight), _hasLeft(other._hasLeft)
{
	std::cout << "Copy constructor\n";
	if (other.getLeft() != nullptr)
	{
		_left = new BSNode(*other.getLeft());
	}
	if (other.getRight() != nullptr)
	{
		_right = new BSNode(*other.getRight());
	}
}

template <class T>
BSNode<T>::~BSNode()
{
	delete _left;
	delete _right;
}

template <class T>
void BSNode<T>::insert(T value)
{
	if (value == _data)
	{
		_count++;
		//return;
	}
	if (_data > value && _hasLeft)
	{
		_left->insert(value);
	}
	else if (_data < value && _hasRight)
	{
		_right->insert(value);
	}
	else if(_data > value && !_hasLeft)
	{
		_left = new BSNode(value);
		_hasLeft = true;
	}
	else if (_data < value && !_hasRight)
	{
		_right = new BSNode(value);
		_hasRight = true;
	}
	
}
template <class T>
BSNode<T>& BSNode<T>::operator=(BSNode* other)
{
	/*_data = other._data;
	_count = other._count;
	_left = other._left;
	_right = other.right;
	
	BSNode* temp = this;
	std::cout << "Memory address of temp is " << std::addressof(temp) << "\n";
	std::cout << "Memory address of this is " << std::addressof(this) << "\n";
	this = new BSNode(other);
	delete temp;
	//BSNode* temp = this;
	//*this = other;
	//delete temp;
	//return *this;
	//other.swap(*this);
	return *this;*/
	swap(*this, *other); // (2)

	return *this;
}
template <class T>
bool BSNode<T>::isLeaf() const
{
	return !hasLeft && !hasRight;
}
template <class T>
bool BSNode<T>::search(T value) const
{
	if (value == _data)
	{
		return true;
	}
	if (_hasLeft && _left->search(value))
	{
		return true;
	}
	if (_hasRight && _right->search(value))
	{
		return true;
	}
	return false;
	
}
template <class T>
T BSNode<T>::getData() const
{
	return this->_data;
}
template <class T>
BSNode<T>* BSNode<T>::getLeft() const
{
	return _left;
}
template <class T>
BSNode<T>* BSNode<T>::getRight() const
{
	return _right;
}
template <class T>
int BSNode<T>::getHeight() const
{
	int num1 = -1, num2 = -1;
	if (_hasLeft)
		num1 = _left->getHeight();
	if (_hasRight)
		num2  = _right->getHeight();
	return max(num1, num2) + 1;
	
}
template <class T>
int BSNode<T>::getDepth(const BSNode& root) const
{
	int num1 = -1, num2 = -1;
	if (this != NULL && root.hasLeft() && root.getLeft()->getData() == _data)
	{
		return 1;
	}
	else if(this != NULL && root.hasRight() && root.getRight()->getData() == _data)
	{
		return 1;
	}
	if(root.hasLeft())
	{
		num1 = getDepth(*root.getLeft());
	}
	if (root.hasRight())
	{
		num2 = getDepth(*root.getRight());
	}
	if (num1 != -1)
	{
		return num1 + 1;
	}
	else if (num2 != -1)
	{
		return num2 + 1;
	}
	else
	{
		return -1;
	}

	
}
template <class T>
bool BSNode<T>::hasLeft() const
{
	return _hasLeft;
}
template <class T>
bool BSNode<T>::hasRight() const
{
	return _hasRight;
}
template <class T>
void BSNode<T>::printNodes() const
{
	if (this == NULL) return;
	_left->printNodes();
	std::cout << _data << " " << _count << std::endl;
	_right->printNodes();
}
template <class T>
void BSNode<T>::setHasRight(bool B)
{
	this->_hasRight = B;
}
template <class T>
void BSNode<T>::setHasLeft(bool B)
{
	this->_hasLeft = B;
}
template <class T>
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode* node) const
{
	return getDepth<T>(node);
}
template <class T>
void swap(BSNode<T>& first, BSNode<T>& second)
{
	using std::swap;

	// by swapping the members of two objects,
	// the two objects are effectively swapped
	std::cout << "Swap\n";
	swap(first._data, second._data);
	swap(first._left, second._left);
	swap(first._right, second._right);
	swap(first._hasLeft, second._hasLeft);
	swap(first._hasRight, second._hasRight);
}
